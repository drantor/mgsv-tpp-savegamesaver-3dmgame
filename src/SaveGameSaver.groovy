/**
 * Created by Flex on 15.09.2015.
 */

import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry
import java.nio.file.Path

DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd--HH-mm-ss")
def cli = new CliBuilder(usage: "savegamesaver")
cli.p(longOpt: "path", "Path to MGSV:TPP", args:1)
cli.t(longOpt: "time", "Time in min. between backups", args:1)

def options = cli.parse(args)

if (!options.p) {
    cli.usage()
    return 0
}

def timer = 30
if (options.t) {
    timer = Integer.parseInt(options.t)
}

def mainPath = Paths.get(options.p)
def saveGamePath = mainPath.resolve("3DMGAME")
if (!Files.exists(saveGamePath)) {
    println("Wrong gamepath?")
    println("Couldn't find: ${saveGamePath.toString()}")
    return 0
}

println("Start thread for creating zips...")
Runnable runnable = {
    println("First we sleep so the game can start...")
    while(true) {
        Thread.sleep(TimeUnit.MINUTES.toMillis(timer))
        Path zipFilePath = mainPath.resolve("savegame--${formatter.format(LocalDateTime.now())}.zip")
        println("ZIP: " + zipFilePath.toString())

        def zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath.toFile()))
        def addDirToZip
        addDirToZip = { dir, parentPath = "" ->
            dir.eachFile {
                file ->
                    zipEntryName = "${parentPath}/${file.getName()}"
                    if (file.isDirectory()) {
                        if (file.list().length != 0) {
                            addDirToZip(file, zipEntryName)
                        }
                    } else {
                        zipOut.putNextEntry(new ZipEntry(zipEntryName))
                        Files.copy(file.toPath(), zipOut)
                        zipOut.closeEntry()
                    }
            }
        }
        addDirToZip(saveGamePath.toFile(), saveGamePath.toFile().getName())

        zipOut.flush()
        zipOut.close()
        println("Now we sleep...")
    }
}
def t = new Thread(runnable)
t.setDaemon(false)
t.start()